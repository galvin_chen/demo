package com.xxl.job.plus.executor.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 魔改xxl-job，彻底告别手动配置任务！
 * @author : woniu
 * @date: 2023/5/03 9:57
 * @version: 1.0
 */
@Configuration
@ComponentScan(basePackages = "com.xxl.job.plus.executor")
public class XxlJobPlusConfig {
}
