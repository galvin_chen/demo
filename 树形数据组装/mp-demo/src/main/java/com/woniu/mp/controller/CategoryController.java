package com.woniu.mp.controller;

import cn.hutool.core.lang.tree.Tree;
import com.woniu.mp.service.CategoryService;
import com.woniu.mp.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("product/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 查询出所有分类以及子分类，以树形结构组装起来列表
     */
    @RequestMapping("/list/tree")
    public R list(){
        List<Tree<Long>> trees = categoryService.listWithTree();
        return R.ok().put("data", trees);
    }



}
