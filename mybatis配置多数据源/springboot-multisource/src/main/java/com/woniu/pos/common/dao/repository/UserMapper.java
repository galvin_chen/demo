package com.woniu.pos.common.dao.repository;

import com.woniu.pos.common.dao.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * @author 蜗牛
 */
public interface UserMapper extends BaseMapper<User> {

}
