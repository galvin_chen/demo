package com.woniu;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;


/**
 * 性能优化-如何爽玩多线程来开发？场景实战！
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class TestA {

    ThreadPoolExecutor executor = new ThreadPoolExecutor(
            20,
            50,
            5,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(200),
            Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.CallerRunsPolicy()
    );

    /**
     * 并行聚合处理数据！
     * 主要运用CompletableFuture.allOf()方法，
     * 将原本串行的操作改为并行。本案例相对比较常规
     */
    @Test
    public void threadDemo1() throws Exception {

        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            //逻辑A
            log.info("A");
            return "A";
        }, executor);

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            //逻辑B
            log.info("B");
            return "B";
        }, executor);

        CompletableFuture<String> future3 = CompletableFuture.supplyAsync(() -> {
            //逻辑C
            log.info("C");
            return "C";
        }, executor);

        CompletableFuture.allOf(future1, future2, future3)
                .handle((result, exception) -> {
                    if (exception != null) {
                        log.error("处理失败：{}", exception);
                    } else {
                        //等待三个任务执行完 接着处理这个逻辑

                    }
                    return null;
                });
    }


    /**
     * 修改for循环为并行操作
     * <p>
     * 这里借鉴了parallelStream流的思路，将串行的for循环分割成多个集合后，对分割后的集合进行循环。这应该是最普遍的多线程应用场景了
     */
    @Test
    public void threadDemo2() throws Exception {

        List<String> slAddList = new ArrayList<>(50000);
        slAddList.addAll(Arrays.asList("1", "2", "3", "4", "5", "6"));

        List<List<String>> partition = Lists.partition(slAddList, 2);

        CompletableFuture.allOf(partition.stream().map(partitionList -> CompletableFuture.runAsync(() -> {
                    log.info("处理逻辑partitionList");
                }, executor)
        ).toArray(CompletableFuture[]::new))
                .whenComplete((res, e) ->
                {
                    if (e != null) {
                        log.error("多线程处理数据失败", e);
                    } else {
                        try {
                            log.info("进一步处理循环后的结果");
                        } catch (Exception ex) {
                            log.error("处理失败", ex);
                        }
                    }
                });

    }

    /**
     * 修改Map遍历为并行操作
     */
    @Test
    public void threadDemo3() throws Exception {
        //代码示例
        Map<String, List<String>> materialMap =  new HashMap<String, List<String>>() {
            {
                put("name", Arrays.asList("a","b","c"));
                put("age", Arrays.asList("1","2","3"));
                put("city", Arrays.asList("nj","bj","cz"));
                put("province", Arrays.asList("js","zj","hn"));
                put("love", Arrays.asList("bk","jk","bk"));
            }
        };
        List<Map<String, List<String>>> mapList = mapSplit(materialMap, 2);
        CompletableFuture<Void> handle =
                CompletableFuture.allOf(mapList.stream().map(splitMap -> CompletableFuture.runAsync(() -> {
                    splitMap.forEach((identity, list) -> {
                       log.info("identity:{},list:{}",identity,list);
                    });
                }, executor)).toArray(CompletableFuture[]::new))
                        .exceptionally(e -> {
                            log.error("多线程组装数据失败", e);
                            return null;
      });

    }





    /**
     * 将map切成段--工具类
     *
     * @param splitMap 被切段的map
     * @param splitNum 每段的大小
     */
    public static <k, v> List<Map<k, v>> mapSplit(Map<k, v> splitMap, int splitNum) {
        if (splitMap == null || splitNum <= 0) {
            List<Map<k, v>> list = new ArrayList<>();
            list.add(splitMap);
            return list;
        }
        Set<k> keySet = splitMap.keySet();
        Iterator<k> iterator = keySet.iterator();
        int i = 1;
        List<Map<k, v>> total = new ArrayList<>();
        Map<k, v> tem = new HashMap<>();
        while (iterator.hasNext()) {
            k next = iterator.next();
            tem.put(next, splitMap.get(next));
            if (i == splitNum) {
                total.add(tem);
                tem = new HashMap<>();
                i = 0;
            }
            i++;
        }
        if (!CollectionUtils.isEmpty(tem)) {
            total.add(tem);
        }
        return total;
    }

}
