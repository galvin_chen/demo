package com.jiaqing.tooldesensitization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: 一个注解解决数据脱敏问题
 * @return:
 * @author: woniu
 * @time: 2023/4/17 21:11
 */
@SpringBootApplication
public class ToolDesensitizationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ToolDesensitizationApplication.class, args);
    }

}
