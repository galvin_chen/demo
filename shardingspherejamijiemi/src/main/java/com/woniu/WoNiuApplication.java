package com.woniu;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@Slf4j
@MapperScan("com.woniu.service.mapper")
public class WoNiuApplication {

    public static void main(String[] args) {
        SpringApplication.run(WoNiuApplication.class, args);
        log.info("test_info");
        log.error("test_error");
        log.warn("test_warn");
    }



}
