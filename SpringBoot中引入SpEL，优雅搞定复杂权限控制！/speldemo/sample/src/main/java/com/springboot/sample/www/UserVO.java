package com.springboot.sample.www;

import lombok.Data;

@Data
public class UserVO {
 private String userId;

 // 拥有的角色名
 private String roleName;
}
