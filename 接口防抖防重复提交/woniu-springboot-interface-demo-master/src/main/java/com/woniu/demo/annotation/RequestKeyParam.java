package com.woniu.demo.annotation;

import java.lang.annotation.*;

/**
 * @className: RequestKeyParam
 * @author: woniu
 * @date: 2024/1/3
 **/
@Target({ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface RequestKeyParam {

}

