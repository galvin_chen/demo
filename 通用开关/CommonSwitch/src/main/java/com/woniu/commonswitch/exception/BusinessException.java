package com.woniu.commonswitch.exception;

/**
 * <p>
 * 自定义业务异常
 *  程序员蜗牛
 * </p>
 */
public class BusinessException extends RuntimeException {

	public BusinessException() {
		super();
	}

	public BusinessException(String errMsg) {
		super(errMsg);
	}

	public BusinessException(String errMsg, Throwable throwable) {
		super(errMsg, throwable);
	}
}
