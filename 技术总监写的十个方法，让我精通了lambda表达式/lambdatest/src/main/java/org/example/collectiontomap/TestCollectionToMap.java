package org.example.collectiontomap;

import com.google.common.collect.Lists;
import org.example.entity.OrderItem;
import org.junit.Test;

import java.util.List;
import java.util.Map;

// 对Java Stream进行二次封装写的10个方法 精通lambda表达式
public class TestCollectionToMap {

    @Test
    public void testToMap() {
        List<OrderItem> orderItems = Lists.newArrayList(
                new OrderItem(1, 5d, "手表"),
                new OrderItem(2, 6d, "机器人"),
                new OrderItem(3, 8d, "手机")
        );
        Map<Integer, OrderItem> map = CollectionToMap.toMap(orderItems, OrderItem::getOrderId);
    }

    @Test
    public void testToMapV2() {
        List<OrderItem> orderItems = Lists.newArrayList(
                new OrderItem(1, 5d, "手表"),
                new OrderItem(2, 6d, "机器人"),
                new OrderItem(3, 8d, "手机")
        );
        Map<Integer, Double> map = CollectionToMap.toMap(orderItems, OrderItem::getOrderId, OrderItem::getPrice);
    }

}
