package com.woniu.logger.sql;

import com.woniu.logger.sql.impl.MySQLDdlSql;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: SQL执行ddl语句工厂 <br/>
 */
@Getter
@AllArgsConstructor
public enum DdlSqlFactory {

    MYSQL(new MySQLDdlSql()),
    ;

    private final IDdlSql ddl;

}

