package com.woniu.logger.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.logger.entity.LoggerInfoEntity;

public interface LoggerInfoMapper extends BaseMapper<LoggerInfoEntity> {
}
