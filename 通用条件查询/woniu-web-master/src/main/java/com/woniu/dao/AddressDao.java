package com.woniu.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.address.Address;

public interface AddressDao extends BaseMapper<Address> {
}
